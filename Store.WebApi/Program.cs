﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nancy;
using Nancy.Hosting.Self;

namespace Store.WebApi
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var host = new NancyHost(new DefaultNancyBootstrapper(),
                new HostConfiguration
                {
                    UrlReservations = new UrlReservations { CreateAutomatically = true, User = "Everyone" }
                },
                new Uri("http://localhost:1234")))
            {
                host.Start();
                Console.WriteLine("Running on http://localhost:1234");
                Console.ReadLine();
            }
        }
    }
}
