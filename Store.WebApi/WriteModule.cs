using System;
using Nancy;
using Nancy.ModelBinding;
using Store.Domain;

namespace Store.WebApi
{
    public class WriteModule : NancyModule
    {
        public WriteModule(Domain.Store store)
        {
            Post["/processOrder"] = _ =>
            {
                var model = this.Bind<OrderModel>();

                try
                {
                    store.ProcessOrder(model.orderItems);
                }
                catch (Exception)
                {
                    return HttpStatusCode.BadRequest;
                }

                return HttpStatusCode.OK;
            };
        }
    }

    public class OrderModel
    {
        public OrderItem[] orderItems;
    }
}