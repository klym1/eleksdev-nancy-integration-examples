﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using Store.Domain;

namespace ExampleProj.Tests
{
    [TestFixture]
    public class StoreTests
    {
        [Test]
        public void When_Items_Array_is_empty_ArgumentException_should_be_thrown()
        {
            var warehouseMock = new Mock<IWarehouse>();

            //Arrange
            var sut = new Store.Domain.Store(warehouseMock.Object, Mock.Of<IShippingService>());

            //Act/Assert
            Assert.Throws<ArgumentException>(() => sut.ProcessOrder());
        }

        [Test]
        public void Service_should_look_for_required_items_in_warehouse()
        {
            var warehouseMock = new Mock<IWarehouse>();

            warehouseMock
                .Setup(m => m.GetAvailability(It.IsAny<int>()))
                .Verifiable();

            //Arrange
            var sut = new Store.Domain.Store(warehouseMock.Object, Mock.Of<IShippingService>());

            //Act
            sut.ProcessOrder(new OrderItem
            {
                CommodityId = 23,
                RequestedAmount = 5
            });

            //Assert
            warehouseMock.Verify(m => m.GetAvailability(23), Times.Once);
        }
    }
}
