﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;

namespace Store.Domain
{
    public class JsonWarehouse : IWarehouse
    {
        private HttpClient client = new HttpClient();
        private string url = "http://localhost:1234/";

        public void Add(int commodityId, uint amount)
        {
            //client.PutAsync($"{url}", new StringContent(am)).Wait();
        }

        public uint GetAvailability(int commodityId)
        {
            throw new System.NotImplementedException();
        }

        public void Remove(int commodityId, uint amount)
        {
            throw new System.NotImplementedException();
        }
    }

    public class Warehouse : IWarehouse
    {
        private readonly Dictionary<int, uint> _itemsInWarehouse = new Dictionary<int, uint>(); 

        public void Add(int commodityId, uint amount)
        {
            if (_itemsInWarehouse.ContainsKey(commodityId))
            {
                _itemsInWarehouse[commodityId] += amount;
            }
            else
            {
                _itemsInWarehouse[commodityId] = amount;
            }
        }

        public uint GetAvailability(int commodityId)
        {
            uint availableItems;
            return _itemsInWarehouse.TryGetValue(commodityId, out availableItems) ? availableItems : 0;
        }

        public void Remove(int commodityId, uint amount)
        {
            if (_itemsInWarehouse.ContainsKey(commodityId))
            {
                _itemsInWarehouse[commodityId] -= amount;
            }
        }
    }
}