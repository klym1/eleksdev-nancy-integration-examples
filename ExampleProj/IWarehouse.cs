﻿namespace Store.Domain
{
    public interface IWarehouse
    {
        void Add(int commodityId, uint amount);
        uint GetAvailability(int commodityId);
        void Remove(int commodityId, uint amount);
    }

    public interface IShippingService
    {
        void ShipToClient(ShipmentInfo info);
    }

    public class ShippingService : IShippingService
    {
        public void ShipToClient(ShipmentInfo info)
        {
            
        }

        public ShippingService()
        {
        }
    }
}
