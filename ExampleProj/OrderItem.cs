﻿namespace Store.Domain
{
    public struct OrderItem
    {
        public int CommodityId { get; set; }
        public uint RequestedAmount { get; set; }
    }
}