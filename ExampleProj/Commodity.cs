﻿namespace Store.Domain
{
    public class Commodity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
    }
}