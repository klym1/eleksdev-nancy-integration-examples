using System;

namespace Store.Domain
{
    public class Store
    {
        private readonly IWarehouse _warehouse;
        private readonly IShippingService _shippingService;

        public Store(IWarehouse warehouse, IShippingService shippingService)
        {
            _warehouse = warehouse;
            _shippingService = shippingService;
        }

        public void ProcessOrder(params OrderItem[] items)
        {
            if(items.Length == 0)
                throw new ArgumentException();

            foreach (var orderItem in items)
            {
                var availableAmount = _warehouse.GetAvailability(orderItem.CommodityId);
                if (availableAmount < orderItem.RequestedAmount)
                    throw new InvalidOperationException(
                        $"Not enough amount of goods in the wearhouse (Id={orderItem.CommodityId}, requstedAmound={orderItem.RequestedAmount}, available={availableAmount})");
            }

            foreach (var orderItem in items)
            {
                _warehouse.Remove(orderItem.CommodityId, orderItem.RequestedAmount);
            }

            _shippingService.ShipToClient(new ShipmentInfo {});
        }
    }
}