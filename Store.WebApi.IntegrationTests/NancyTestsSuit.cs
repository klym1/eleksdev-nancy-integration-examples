﻿using FluentAssertions;
using Nancy;
using Nancy.Testing;
using NUnit.Framework;

namespace Store.WebApi.IntegrationTests
{
    [TestFixture]
    public class NancyTestsSuit
    {
        [Test]
        public void Test1()
        {
            //Arrange
            var br = new Browser(conf =>
            {
                conf.Dependency<Domain.Store>();
                conf.Modules(typeof(WriteModule));
                conf.EnableAutoRegistration();
            });

            //Act
            var response = br.Post("/processOrder", ctx =>
            {
                ctx.Header("Accept", "application/json");
            });

            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }
    }
}
